/* This file need the option "-lpthread" to be compiled succesfully by gcc.
 * Definition of the declared functions o nthis file is found in "input_thread.c".*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


/* DEATH_LETTER is the value for which the main process and threads (the game) reset/kill themselves.*/
#define DEATH_LETTER 'l'

/* This function reads the user input without the need for pressing line break ("enter")
after each input. This is achieved by chaning properties of the terminal i/o.
 * For more information, check "man stty" documentation.*/
void *input_reader(void* vargp);

/* This function creates thread responsible for reading inputs if it has not been yet created once.
 * It checks for the global static variable "thread_num" to check whether a thread has already been created.
 * Caller is still responsable for killing the thread at the end of execution by either calling 
pthread_kill(pthread_t thread, int sig) or by the used inputing DEATH_LETTER.
 * OUTPUT: id of the created thread.*/
pthread_t GO_input_thread();

/* This function returns the last input given by the user and clears it.
 * OUTPUT: user's last input.*/
char get_input();