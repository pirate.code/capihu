#include "stone.h"
#include "button_wall.h"
#include "players.h"
#include<stdio.h>

struct stone g_stone;
extern struct player g_thief;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.
 * Time is used to control the speed of the thrown stone and the control attribute controls 
 whether the stone has already moved in a given "tik cycle".*/
static int _stone_update(struct object *o, t_stage st)
{
    struct stone *stone_ptr = (struct stone*)o;
    int horizontal = 0, vertical = 0;
    struct timespec time;
    long ntime;
    long diff;

    if(stone_ptr->grabbed == 1)
    {
        stone_ptr->pos_x = g_thief.pos_x+1;
        stone_ptr->pos_y = g_thief.pos_y;
        if(stone_ptr->init == 0)
        {
            stone_ptr->init = 1;
            return 1;
        }
        else
            return 0;
    }

    if(stone_ptr->state == STONE_NOT_MOVING)
        {
            st[stone_ptr->pos_y][stone_ptr->pos_x] = O_STONE;
            return 0;
        }
    if(stone_ptr->state == STONE_UP)     vertical = -1;
    if(stone_ptr->state == STONE_DOWN)   vertical = +1;
    if(stone_ptr->state == STONE_LEFT)   horizontal = -1;
    if(stone_ptr->state == STONE_RIGHT)  horizontal = +1;
    
    timespec_get(&time, TIME_UTC); //gets time in nanoses
    ntime = time.tv_nsec / 10000000;
    diff = ntime % (STONE_MOV_UPDATE_TIME);

    if(diff == 0 && stone_ptr->already_moved_this_cycle == 0)
    {
        stone_ptr->already_moved_this_cycle = 1;
        if(st[stone_ptr->pos_y+vertical][stone_ptr->pos_x+horizontal] == O_EMPTY || st[stone_ptr->pos_y+vertical][stone_ptr->pos_x+horizontal] == O_FIRE)
        {
            st[stone_ptr->pos_y][stone_ptr->pos_x] = O_EMPTY;
            stone_ptr->pos_y = stone_ptr->pos_y + vertical;
            stone_ptr->pos_x = stone_ptr->pos_x + horizontal;
        }
        else if(st[stone_ptr->pos_y+vertical][stone_ptr->pos_x+horizontal] == O_BUTTON_WALL)
        {
            button_wall_change_state(BUTTON_WALL_ON);
            stone_ptr->pos_y = stone_ptr->pos_y + vertical;
            stone_ptr->pos_x = stone_ptr->pos_x + horizontal;
            stone_ptr->state = STONE_NOT_MOVING;
        }
        else
            stone_ptr->state = STONE_NOT_MOVING;
    }
    if(diff > 0)
    {
        stone_ptr->already_moved_this_cycle = 0;
        return 0;
    }

    st[stone_ptr->pos_y][stone_ptr->pos_x] = O_STONE;
    return 1;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _stone_draw(struct object *o, struct screen *s)
{
    struct stone *stone_ptr = (struct stone*)o;
    
    s->c[stone_ptr->pos_y][stone_ptr->pos_x] = STONE_COLOUR;
    s->m[stone_ptr->pos_y][stone_ptr->pos_x] = STONE_VISUAL;
    
}

/* This function erases the object *o from stage.*/
static void _stone_destroy(struct object *o, t_stage st)
{
    struct stone *stone_ptr = (struct stone*)o;
    if (!stone_ptr->init) return;
        st[stone_ptr->pos_y][stone_ptr->pos_x] = O_EMPTY;
    stone_ptr->init = 0;
}

/* This function is called by the thief when it throws the stone. It sets the direction 
and initial position of the stone.*/
void change_stone_state_and_grab_state(enum stone_state state,int grabbed, int pos_x, int pos_y)
{
    g_stone.state = state;
    g_stone.grabbed = grabbed;
    g_stone.pos_x = pos_x;
    g_stone.pos_y = pos_y;
}

/* This function is called by the mage when it summon the magic. It sets the direction 
and* initial position of the magic.*/
struct stone stone_init(int pos_x, int pos_y)
{
    struct stone s;
    s.o.update = _stone_update;
    s.o.draw = _stone_draw;
    s.o.destroy = _stone_destroy;
    s.pos_x = pos_x;
    s.pos_y = pos_y;
    s.state = STONE_NOT_MOVING;
    s.already_moved_this_cycle = 0;
    s.grabbed = 0;
    return s;
}
