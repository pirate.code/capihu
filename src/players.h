#ifndef PLAYERS_HEADER
#define PLAYERS_HEADER

#include "object.h"
#include "input_handler.h"

struct player
{
    struct object o;
    int pos_x, pos_y;
    char selected, previous, letter;
    enum facing dir;
    enum obj_id id;
    enum colour c, sc;
    enum p_input input;
};

void
knight_special(t_stage st);

void
mage_special(t_stage st);

void
thief_special(t_stage st);

void
knight_init(int pos_x, int pos_y, t_stage st);

void
mage_init(int pos_x, int pos_y, t_stage st);

void
thief_init(int pos_x, int pos_y, t_stage st);

#endif
