#include "door_key.h"

struct door_key g_door_key;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int
_door_key_update(struct object *o, t_stage st)
{
    (void)o;
    if (g_door_key.init)
    {
        g_door_key.init = 0;
        for(int i = -1; i <= 1; i++)
            st[g_door_key.pos_y][i + g_door_key.pos_x] = O_DOOR_KEY;
        return 1;
    }
    if (g_door_key.update)
    {
        g_door_key.update = 0;
        for(int i = -1; i <= 1; i++)
            st[g_door_key.pos_y][i + g_door_key.pos_x] = O_EMPTY;
        return 1;
    }
    return 0;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void
_door_key_draw(struct object *o, struct screen *s)
{
    struct door_key *door_key_ptr = (struct door_key*)o;
    
    if (g_door_key.real)
    {
        s->c[door_key_ptr->pos_y][door_key_ptr->pos_x-1] = DOOR_KEY_COLOUR1;
        s->m[door_key_ptr->pos_y][door_key_ptr->pos_x-1] = DOOR_KEY_VISUAL1;
        s->c[door_key_ptr->pos_y][door_key_ptr->pos_x] = DOOR_KEY_COLOUR2;
        s->m[door_key_ptr->pos_y][door_key_ptr->pos_x] = DOOR_KEY_VISUAL2;
        s->c[door_key_ptr->pos_y][door_key_ptr->pos_x+1] = DOOR_KEY_COLOUR1;
        s->m[door_key_ptr->pos_y][door_key_ptr->pos_x+1] = DOOR_KEY_VISUAL1;
    }
}

/* This function erases the object *o from stage.*/
static void
_door_key_destroy(struct object *o, t_stage st)
{
    struct door_key *door_key_ptr = (struct door_key*)o;
    
    for(int i = 0; i < 3; i++)
    	st[door_key_ptr->pos_y][i + door_key_ptr->pos_x] = O_EMPTY;
}

/* This function changes init attribute from door_key and it is called by the thief when
he/she touches the door with a key.*/
void door_key_unlock(void)
{
    g_door_key.update = 1;
    g_door_key.real = 0;
}

/* Object constructor */
struct door_key
door_key_init(int pos_x, int pos_y)
{
    struct door_key d;
    d.o.update = _door_key_update;
    d.o.draw = _door_key_draw;
    d.o.destroy = _door_key_destroy;
    d.pos_x = pos_x;
    d.pos_y = pos_y;
    d.update = 0;
    d.init = 1;
    d.real = 1;
    return d;
}
