#include "wall.h"

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int _wall_update(struct object *o, t_stage st)
{
    struct wall *w = (struct wall*)o;
    if (w->init) return 0;
    if (w->ori == HORIZONTAL)
        for (int i = w->pos_start; i < w->pos_end; i++)
            st[w->pos_fix][i] = O_WALL;
    else
        for (int i = w->pos_start; i < w->pos_end; i++)
            st[i][w->pos_fix] = O_WALL;
    w->init = 1;
    return 1;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _wall_draw(struct object *o, struct screen *s)
{
    struct wall *w = (struct wall*)o;
    if (w->ori == HORIZONTAL)
    {
        for (int i = w->pos_start; i < w->pos_end; i++)
        {
            s->c[w->pos_fix][i] = WALL_COLOUR;
            s->m[w->pos_fix][i] = WALL_VISUAL;
        }
    }
    else
    {
        for (int i = w->pos_start; i < w->pos_end; i++)
        {
            s->c[i][w->pos_fix] = WALL_COLOUR;
            s->m[i][w->pos_fix] = WALL_VISUAL;
        }
    }
}

/* This function erases the object *o from stage.*/
static void _wall_destroy(struct object *o, t_stage st)
{
    struct wall *w = (struct wall*)o;
    if (!w->init) return;
    if (w->ori == HORIZONTAL)
        for (int i = w->pos_start; i < w->pos_end; i++)
            st[w->pos_fix][i] = O_EMPTY;
    else
        for (int i = w->pos_start; i < w->pos_end; i++)
            st[i][w->pos_fix] = O_EMPTY;
    w->init = 0;
}

/* Object constructor */
struct wall wall_init(enum w_orientation ori, int pos_fix,
          int pos_start, int pos_end)
{
    struct wall w;
    w.o.update = _wall_update;
    w.o.draw = _wall_draw;
    w.o.destroy = _wall_destroy;
    w.ori = ori;
    w.pos_fix = pos_fix;
    w.pos_start = pos_start;
    w.pos_end = pos_end;
    return w;
}
