#ifndef WALL_OBJECTS_HEADER
#define WALL_OBJECTS_HEADER

#include "object.h"

#define WALL_VISUAL '#'
#define WALL_COLOUR GRY

/* create the wall objects */

enum w_orientation
{
    HORIZONTAL,
    VERTICAL,
};

/* wall objects.
 * orientation determines if it is vertical or horizontal.
 * if vertical, pos_fix is the COLUMN and pos_start and end
 * are the start and end of the wall LINES.
 * if horizontal swap COLUMN for lines */
struct wall
{
    struct object o;
    enum w_orientation ori;
    int
        pos_fix,
        pos_start,
        pos_end;
    char init;
};

struct wall
wall_init(enum w_orientation ori, int pos_fix,
          int pos_start, int pos_end);

#endif
