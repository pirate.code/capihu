#include "door_button_wall.h"

struct door_button_wall g_door_button_wall;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int
_door_button_wall_update(struct object *o, t_stage st)
{
    struct door_button_wall *door_button_wall_ptr = (struct door_button_wall*)o;

    if(door_button_wall_ptr->state == DOOR_BUTTON_WALL_OPEN)
    {
        for(int i = -1; i <= 1; i++)
            st[door_button_wall_ptr->pos_y][i + door_button_wall_ptr->pos_x] = O_EMPTY;
        if(door_button_wall_ptr->init == 0)
        {
            door_button_wall_ptr->init = 1;
            return 1;
        }
        else return 0;
    }
    else
    {
	    for(int i = -1; i <= 1; i++)
	    	st[door_button_wall_ptr->pos_y][i + door_button_wall_ptr->pos_x] = O_DOOR_BUTTON_WALL;
        if(door_button_wall_ptr->init == 0)
        {
            door_button_wall_ptr->init = 1;
            return 1;
        }
        else return 0;
    }
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void
_door_button_wall_draw(struct object *o, struct screen *s)
{
    struct door_button_wall *door_button_wall_ptr = (struct door_button_wall*)o;

    if(door_button_wall_ptr->state == DOOR_BUTTON_WALL_OPEN)
    {
        /*s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = NRM;
        s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = 0;
        s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x-1] = NRM;
        s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x-1] = 0;
        s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = NRM;
        s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = 0;*/
        return;
    }

    s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x-1] = DOOR_BUTTON_WALL_COLOUR1;
    s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x-1] = DOOR_BUTTON_WALL_VISUAL1;
    s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = DOOR_BUTTON_WALL_COLOUR2;
    s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x] = DOOR_BUTTON_WALL_VISUAL2;
    s->c[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x+1] = DOOR_BUTTON_WALL_COLOUR1;
    s->m[door_button_wall_ptr->pos_y][door_button_wall_ptr->pos_x+1] = DOOR_BUTTON_WALL_VISUAL1;
}

/* This function erases the object *o from stage.*/
static void
_door_button_wall_destroy(struct object *o, t_stage st)
{
    struct door_button_wall *door_button_wall_ptr = (struct door_button_wall*)o;
    
    for(int i = 0; i < 3; i++)
    	st[door_button_wall_ptr->pos_y][i + door_button_wall_ptr->pos_x] = O_EMPTY;

    door_button_wall_ptr->init =0;
}

/* this function destroys the door. It is called by the stone or by magic objects. */
void door_button_wall_unlock()
{
    g_door_button_wall.state = DOOR_BUTTON_WALL_OPEN;
}

/*
void door_button_wall_relock(t_stage st)
{
    g_door_button_wall.init = 1;
}
*/


/* Object constructor. */
struct door_button_wall
door_button_wall_init(int pos_x, int pos_y)
{
    struct door_button_wall d;
    d.o.update = _door_button_wall_update;
    d.o.draw = _door_button_wall_draw;
    d.o.destroy = _door_button_wall_destroy;
    d.pos_x = pos_x;
    d.pos_y = pos_y;
    d.init = 0;
    d.state = DOOR_BUTTON_WALL_CLOSED;
    return d;
}
