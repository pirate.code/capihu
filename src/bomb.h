#ifndef BOMB_OBJECTS_HEADER
#define BOMB_OBJECTS_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define BOMB_VISUAL1 '@'
#define BOMB_VISUAL2 '~'
#define BOMB_VISUAL3 '*'
#define BOMB_COLOUR1 B_MAG
#define BOMB_COLOUR2 B_MAG
#define BOMB_COLOUR3 BLINKING_B_RED

/* bomb objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct bomb
{
    struct object o;
    int pos_x, pos_y;
    char init;
};

struct bomb
bomb_init(int pos_x, int pos_y);

#endif